#include <wx/wx.h>
#include "fmod/fmod.h"
#include "okno.h"

class MojaAplikacja : public wxApp
{
public:
	virtual bool OnInit();
};


IMPLEMENT_APP(MojaAplikacja)	


bool MojaAplikacja::OnInit()
{
	if ( !wxApp::OnInit() )
		return false;
	FSOUND_Init (44100, 16, 0);

	Okno *okno = new Okno(_T("Snake"));
	okno->SetWindowStyle(wxDEFAULT_FRAME_STYLE & ~wxRESIZE_BORDER);
	okno->Centre();
	okno->Show(true); 

	return true;
}