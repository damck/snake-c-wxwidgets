#include "okno.h"

Okno::Okno(const wxString& title)
	: wxFrame(NULL, wxID_ANY, title, wxDefaultPosition)
{
	panel = new Panel(this);

	wxMenu *gra = new wxMenu;
	wxMenu *pomoc = new wxMenu;

	pomoc->Append(wxID_ABOUT, wxT("&O programie...\tF1"));
	pomoc->Append(wxID_HELP, wxT("&Sterowanie\tF2"));

	gra->Append(wxID_NEW, wxT("&Nowa gra\tAlt-N"));
	gra->Append(wxID_EXIT, wxT("&Wyjscie\tAlt-W"));

	wxMenuBar *menuBar = new wxMenuBar();
	menuBar->Append(gra, wxT("&Gra"));
	menuBar->Append(pomoc, wxT("P&omoc"));
	SetMenuBar(menuBar);

	Connect(wxID_EXIT, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(Okno::OnWyjdz));
	Connect(wxID_ABOUT, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(Okno::OnO));
	Connect(wxID_NEW, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(Okno::OnNowaGra));
	Connect(wxID_HELP, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(Okno::OnPomoc));
	
	SetSize(wxSize(600+16, 600+menuBar->GetSize().GetHeight() + 56));

	panel->SetFocus();
	panel->tworzPanel();
	panel->SetBackgroundColour(wxColour("#A5C68F"));
	panel->NowaGra(); 
}

void Okno::OnO(wxCommandEvent& event)
{
	wxString msg =  wxT("Gra Snake 2013\nAutorzy:\nDamian Goworko\nMiko�aj Buszko");
	wxMessageBox(msg, wxT("O programie"), wxOK | wxICON_INFORMATION, this);
}



void Okno::OnPomoc(wxCommandEvent& event)
{
	wxString msg =  wxT("Sterowanie:\nStrza�ki - Ruch\np - Pauza");
	wxMessageBox(msg, wxT("Pomoc"), wxOK | wxICON_INFORMATION, this);
}

void Okno::OnWyjdz(wxCommandEvent& event)
{
	Close();
}

void Okno::OnNowaGra(wxCommandEvent& event)
{
	panel->tworzPanel();
	panel->NowaGra(); 
}