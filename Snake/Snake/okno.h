#include <wx/wx.h>
#include "panel.h"

class Okno : public wxFrame
{
public:
	Okno(const wxString& title);

	void OnWyjdz(wxCommandEvent& event);
	void OnO(wxCommandEvent& event);
	void OnNowaGra(wxCommandEvent& event);
	void OnPomoc(wxCommandEvent& event);
private:
	Panel *panel;

};



