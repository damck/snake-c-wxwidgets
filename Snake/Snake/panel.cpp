#include "panel.h"

Panel::Panel(wxFrame* parent)
	: wxPanel(parent, wxID_ANY, wxDefaultPosition,
	wxSize(600, 600), wxBORDER_NONE), snake(3) {
		timer = new wxTimer(this, 1);

		theme=FSOUND_Sample_Load (0,"theme.mp3",FSOUND_LOOP_NORMAL,0,0);
		s_jedz=FSOUND_Sample_Load (1,"jedz.wav",FSOUND_LOOP_OFF,0,0);
		s_gameover=FSOUND_Sample_Load (2,"gameover.mp3",FSOUND_LOOP_OFF,0,0);
		isStart = false; 
		isPauza = false;
		isGameOver = false;
		Connect(wxEVT_PAINT, wxPaintEventHandler(Panel::onPaint));
		Connect(wxEVT_KEY_DOWN, wxKeyEventHandler(Panel::onKeyDown));
		Connect(wxEVT_TIMER, wxCommandEventHandler(Panel::onTimer));
		TimerSpeed = 0;
		TimerMax = 50;
		TimerStart = 100;
		timer->Start(TimerStart);
}

//zaczyna nowa gre, ustawia niektore zmienne do wartosci poczatkowych
void Panel::NowaGra()
{
	FSOUND_StopSound (2);
	FSOUND_PlaySound (0,theme);
	FSOUND_SetVolume (0, 50);

	isGameOver = false;
	isStart = true;
	TimerSpeed = 0;
	TimerStart = 100;
	timer->Start(TimerStart);
	snake.tworzSnake();
	Refresh();
}

//Pauzuje gre zatrzymujac timer
void Panel::Pauza()
{
	if (!isStart)
		return;

	isPauza = !isPauza;
	if (isPauza) {
		FSOUND_SetPaused (0, true);
		timer->Stop();
	} else {
		FSOUND_SetPaused (0, false);
		timer->Start(TimerStart);
	}
	Refresh();
}

//wypelnia tablice planszy jedzeniem
void Panel::tworzPanel()
{
	int i, j;
	int iloscPKT;

	for (i = 0; i < 30; i=i+1) {
		for (j = 0; j < 30; j=j+1) {
			iloscPKT = rand() % 90;
			skladPanel[i][j] = (iloscPKT == 0);
		}
	}
}


//generuje jedzenie jezeli zostalo zjedzone
void Panel::generJedzenie()
{
	int x, y;
	do {
		x = rand() % 30;
		y = rand() % 30;
	} while (snake.isSnake(x, y) || skladPanel[x][y] != 0);

	skladPanel[x][y] = 1;
}


//rysuje plansze, wraz z jedzeniem
void Panel::rysujPanel(wxPaintDC& dc)
{
	for (int i = 0; i < 30; i=i+1) {
		for (int j = 0; j < 30; j=j+1) {
			if (skladPanel[i][j] != 0) {
				rysujJedzenie(dc, i, j);
			}
		}
	}
}


//metoda rysowania sztuki jedzenia
void Panel::rysujJedzenie(wxPaintDC& dc, int x, int y)
{
	wxColour body = "#7C6E07";
	wxColour border = "#7C6E64";

	wxBrush brush(body);
	dc.SetBrush(brush);

	dc.SetPen(wxPen(border, 1, wxDOT_DASH));

	dc.DrawRectangle(x * 20, y * 20, 20, 20);
}

//metoda konczaca gre
void Panel::GameOver()
{
	FSOUND_StopSound (0);
	FSOUND_StopSound (1);
	timer->Stop();
	FSOUND_PlaySound (2,s_gameover);
	FSOUND_SetVolume (2, 150);

	isGameOver = true;
	Refresh();
}

//metoda rysujaca ekran gameover
void Panel::rysujGameOver(wxPaintDC& dc)
{
	wxColour bg = "#7C6E64";
	wxColour text = "#000000";

	dc.SetBrush(wxBrush(bg));
	dc.SetPen(wxPen(text, 1, wxSOLID));
	dc.DrawRectangle(0, 0, 600, 600);

	wxString msg = wxT("Koniec gry!\n"); 
	wxString msg2 = wxT("Tw�j wynik to: ");
	wxString msg3 = wxString::Format(wxT("%i"),snake.polSnake.size()-3);
	int textX = (600 / 2) - ((msg.Len()/2 - 1) * dc.GetCharWidth());
	int textY = (600 / 2) - dc.GetCharHeight();
	dc.DrawText(msg+msg2+msg3, textX, textY);
}

//metoda obslugujaca zdarzenia timer
void Panel::onTimer(wxCommandEvent& event)
{
	if (snake.zrobRuch() < 0) {
		GameOver();
		return;
	}

	if (snake.getGlowaX() < 0 || snake.getGlowaY() < 0 ||
		snake.getGlowaX() >= 30 || snake.getGlowaY() >= 30) {
			GameOver();
			return;
	}

	if (skladPanel[snake.getGlowaX()][snake.getGlowaY()]) {
		snake.jedz();
		FSOUND_PlaySound (1,s_jedz);
		FSOUND_SetVolume (1, 255);

		skladPanel[snake.getGlowaX()][snake.getGlowaY()] = 0;
		generJedzenie();
	}

	TimerSpeed=TimerSpeed+1;

	if ((TimerSpeed % 10) == 0) {
		TimerStart=TimerStart-2;
		if(TimerStart > TimerMax){
			timer->Start(TimerStart);
		}else
		{
			timer->Start(TimerMax);
		}
		TimerSpeed = 0; 
	}

	Refresh();
}

//metoda obslugujaca zdarzenia paint
void Panel::onPaint(wxPaintEvent& event)
{
	wxPaintDC dc(this);

	if (isGameOver) {
		rysujGameOver(dc);
		return;
	}

	rysujPanel(dc);
	snake.rysujSnake(dc);
}

//metoda obslugujaca klawisze
void Panel::onKeyDown(wxKeyEvent& event)
{
	int keycode = event.GetKeyCode();

	switch (keycode) {
	case 'p':
		Pauza();
		return;
	case 'P':
		Pauza();
		return;
	case WXK_LEFT:
		snake.ruchLewo();
		break;
	case WXK_RIGHT:
		snake.ruchPrawo();
		break;
	case WXK_DOWN:
		snake.ruchDol();
		break;
	case WXK_UP:
		snake.ruchGora();
		break;
	case WXK_ESCAPE:
		exit(0);
		break;
	default:
		event.Skip();
	}
	if (isPauza)
		return;
}
