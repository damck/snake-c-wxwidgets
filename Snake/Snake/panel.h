#include <wx/wx.h>
#include <wx/sound.h>
#include "fmod/fmod.h"
#include "snake.h"

class Panel : public wxPanel
{
public:
	Panel(wxFrame* parent);
	
	void NowaGra();
	void Pauza();
	void tworzPanel();
	void rysujPanel(wxPaintDC& dc);
	void rysujJedzenie(wxPaintDC& dc, int x, int y);
	void rysujGameOver(wxPaintDC& dc);
	void generJedzenie();
	void GameOver();

	bool isStart;
	bool isPauza;

	void onTimer(wxCommandEvent& event);
	void onPaint(wxPaintEvent& event);
	void onKeyDown(wxKeyEvent& event);
private:
	int skladPanel[30][30];
	int TimerSpeed; 
	int TimerStart; 
	int TimerMax;

	bool isGameOver; 

	wxTimer *timer;

	Snake snake;
	FSOUND_SAMPLE* s_jedz;
	FSOUND_SAMPLE* theme;
	FSOUND_SAMPLE* s_gameover;

};