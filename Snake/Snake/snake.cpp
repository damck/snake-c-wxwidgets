#include "snake.h"

Snake::Snake(int dlugoscSnake)
{
	startDlugosc = dlugoscSnake; 
	tworzSnake();
	zjadl = false;
}

//tworzenie nowego weza
void Snake::tworzSnake()
{
	int i = 0;
	polSnake.clear();

	for (int i = 0 ; i < startDlugosc; i=i+1) {
		polSnake.push_back(make_pair(30/2, 30/2));
	}

	ruchX = 0;
	ruchY = 1;
}

//metoda rysujaca elementy weza
void Snake::rysujSnake(wxPaintDC& dc) 
{
	wxColour body = "#7C6E07";
	wxColour border = "#7C6E64";

	wxBrush brush(body);
	dc.SetBrush(brush);

	dc.SetPen(wxPen(border, 1, wxSOLID));	

	for (vector<pair<int, int> >::const_iterator i = polSnake.begin();
		i != polSnake.end(); i=i+1) {
		dc.DrawRectangle(i->first * 20, i->second * 20, 
			20, 20);
	}
}

//metoda odpowiedzialna za ruch, 
//sprawdza tez czy waz nie wpadl na siebie lub czy trafil na jedzenie
int Snake::zrobRuch()
{
	int rozSnake = polSnake.size();
	pair<int, int> oldSnake = polSnake[rozSnake - 1];

	for (int i = rozSnake - 1; i != 0; i=i-1) {
		polSnake[i] = polSnake[i-1];
	}
	
	if (isSnake(polSnake[0].first - ruchX, polSnake[0].second - ruchY)) {
		return -1;
	}

	if (zjadl) {
		polSnake.push_back(oldSnake);
		zjadl = false; 
	}
	

	polSnake[0].first = polSnake[0].first - ruchX;
	polSnake[0].second = polSnake[0].second - ruchY;

	return 0;
}

//metoda mowi czy w danych wspolrzednych jest waz
bool Snake::isSnake(int x, int y)
{
	for (vector<pair<int, int> >::const_iterator i = polSnake.begin();
		i != polSnake.end(); i=i+1) {
		if (x == i->first && y == i->second) {
			return true;
		}
	}

	return false;
}