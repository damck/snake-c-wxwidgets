#include <wx/wx.h>
#include <vector>
#include "fmod/fmod.h"
using namespace std;

class Snake
{
public:
	Snake(int dlugoscSnake);
	void tworzSnake(); 
	void rysujSnake(wxPaintDC& dc);
	void ruchLewo(){
		if (ruchX != -1){
			ruchX = 1;
		}
		ruchY = 0;
	}
	void ruchPrawo(){
		if (ruchX != 1) {
			ruchX = -1;
		}  ruchY = 0;
	}
	void ruchGora(){
		ruchX = 0;  
		if (ruchY != -1){
			ruchY = 1;}
	}
	void ruchDol(){
		ruchX = 0;
		if (ruchY != 1){
			ruchY = -1;
		}
	}
	int zrobRuch();
	int getGlowaX(){
		return polSnake[0].first;
	}
	int getGlowaY(){
		return polSnake[0].second;
	}
	void jedz(){
		zjadl = true;
}
	bool isSnake(int x, int y);
	vector<pair<int, int>> polSnake;

private:

	bool zjadl;
	int ruchX, ruchY;
	int startDlugosc;
};
